//
//  SelicViewController.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 16/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Google
import Charts
import Firebase

class SelicViewController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource, ChartViewDelegate {

	@IBOutlet weak var graficoView: LineChartView!
	@IBOutlet weak var segmentControl: ADVSegmentedControl!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var floatingGraphReader: FloatingGraphReader!

	var selicArray = [Selic]()
	var graficoDatas = [String]()
	var graficoTaxas = [Double]()
	var currentSubLayer: CAShapeLayer!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

		tableView.delegate = self
		tableView.dataSource = self
		self.tableView.rowHeight = 28.0
		self.tableView.estimatedRowHeight = 28.0

		graficoView.delegate = self
		graficoView.descriptionText = ""
		graficoView.noDataText = "Carregando dados..."
		graficoView.descriptionTextColor = UIColor.grayColor()

		floatingGraphReader.hidden = true

		initObeservers(7)

		segmentControl.items = ["6 mes", "1 ano", "3 anos", "6 anos", "10+ anos"]
		segmentControl.font = UIFont(name: "Avenir-Black", size: 11)
		segmentControl.borderColor = UIColor(white: 1.0, alpha: 0.3)
		segmentControl.selectedIndex = 0
		segmentControl.alpha = 0.5
		segmentControl.addTarget(self, action: #selector(SelicViewController.segmentValueChanged(_:)), forControlEvents: .ValueChanged)
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(true)
		self.screenName = "Selic View Controller"
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return selicArray.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let cell = tableView.dequeueReusableCellWithIdentifier("SelicCell") as? SelicCell {

			let selic = self.selicArray[indexPath.row]

			cell.configureCell(selic.vigencia, taxa: selic.taxa)
			return cell
		}
		return SelicCell()
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func initObeservers(periodo: Int?) {

		DataService.ds.REF_SELIC.queryLimitedToLast(UInt(periodo!)).observeSingleEventOfType(.Value, withBlock: { snapshot in

			if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {

				self.selicArray = []
				self.graficoDatas = []
				self.graficoTaxas = []

				for snap in snapshots {
					var data = "", dataCheia = "", taxa = ""

					if let selicDict = snap.value as? Dictionary<String, AnyObject> {
						if let tax = selicDict["ano"] as? String {
							taxa = tax
						}
						if let dat = selicDict["data"] as? String {
							data = dat
						}
						if let datCheia = selicDict["datacheia"] as? String {
							dataCheia = datCheia
						}

						// print(taxa + data + dataCheia)

						let selic = Selic(vigencia: dataCheia, taxa: taxa)
						self.selicArray.append(selic)

						if taxa != "" {
							self.graficoDatas.append(data)

							let tax = taxa.replace(",", withString: ".")
							if let cot = (tax as? NSString)?.doubleValue {
								self.graficoTaxas.append(cot)
							} else {
								self.graficoTaxas.append(0.0)
							}
						}
					}
				}
			}
			self.selicArray = self.selicArray.reverse()
			self.tableView.reloadData()

			self.setChart(self.graficoDatas, valores: self.graficoTaxas)
		})
	}

	func segmentValueChanged(sender: AnyObject?) {

		if segmentControl.selectedIndex == 0 {
			initObeservers(7)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 1 {
			initObeservers(13)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 2 {
			initObeservers(39)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 3 {
			initObeservers(78)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 4 {
			initObeservers(999)
			limpaGrafico()
		} else {
			initObeservers(7)
		}
	}

	func setChart(dataPoints: [String], valores: [Double]) {

		var dataEntries: [ChartDataEntry] = []

		for i in 0 ..< dataPoints.count {
			let dataEntry = ChartDataEntry(value: valores[i], xIndex: i)
			dataEntries.append(dataEntry)
		}

		let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "% a.a")
		lineChartDataSet.lineWidth = 1.5
		lineChartDataSet.drawCirclesEnabled = true
		lineChartDataSet.circleRadius = 1.7
		// lineChartDataSet.setCircleColor(NSUIColor.yellowColor())
		lineChartDataSet.drawCubicEnabled = true
		lineChartDataSet.drawValuesEnabled = false
		lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
		lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
		lineChartDataSet.drawFilledEnabled = true
		lineChartDataSet.fillAlpha = 0.1

		let lineChartData = LineChartData(xVals: dataPoints, dataSet: lineChartDataSet)
		graficoView.data = lineChartData

		graficoView.leftAxis.valueFormatter = NSNumberFormatter()
		graficoView.rightAxis.valueFormatter = NSNumberFormatter()
		graficoView.leftAxis.valueFormatter?.maximumFractionDigits = 2
		graficoView.rightAxis.valueFormatter?.maximumFractionDigits = 2
		graficoView.leftAxis.valueFormatter?.minimumFractionDigits = 2
		graficoView.rightAxis.valueFormatter?.minimumFractionDigits = 2

		graficoView.rightAxis.gridColor = UIColor.lightGrayColor()
		graficoView.leftAxis.gridColor = UIColor.lightGrayColor()
		graficoView.xAxis.gridColor = UIColor.clearColor()

		graficoView.rightAxis.labelTextColor = UIColor.lightGrayColor()
		graficoView.leftAxis.labelTextColor = UIColor.lightGrayColor()
		graficoView.xAxis.labelTextColor = UIColor.lightGrayColor()

		graficoView.animate(xAxisDuration: 1.0, yAxisDuration: 0.0)
		graficoView.dragEnabled = true
		graficoView.pinchZoomEnabled = true
		graficoView.highlightPerDragEnabled = false
	}

	func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {

		let markerPosition = graficoView.getMarkerPosition(entry: entry, highlight: highlight)

		createCircle(markerPosition.x, y: markerPosition.y)

		// Adding top marker
		floatingGraphReader.taxaLbl.text = "\(entry.value)%"
		floatingGraphReader.dataLbl.text = "\(self.graficoDatas[entry.xIndex])"
		floatingGraphReader.center = CGPointMake(markerPosition.x, markerPosition.y + 20)
		floatingGraphReader.hidden = false
	}

	func chartValueNothingSelected(chartView: ChartViewBase) {
		limpaGrafico()
	}

	func createCircle(x: CGFloat, y: CGFloat) {
		let circlePath = UIBezierPath(arcCenter: CGPoint(x: x, y: y), radius: CGFloat(2), startAngle: CGFloat(0), endAngle: CGFloat(M_PI * 2), clockwise: true)

		let shapeLayer = CAShapeLayer()
		shapeLayer.path = circlePath.CGPath

		// change the fill color
		shapeLayer.fillColor = UIColor.clearColor().CGColor
		// you can change the stroke color
		shapeLayer.strokeColor = UIColor.redColor().CGColor
		// you can change the line width
		shapeLayer.lineWidth = 1.5

		if self.currentSubLayer != nil {
			self.currentSubLayer.removeFromSuperlayer()
		}
		graficoView.layer.addSublayer(shapeLayer)
		self.currentSubLayer = shapeLayer
	}

	@IBAction func backBtn(sender: AnyObject) {
		self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer()) }
	@IBAction func menuBtn(sender: AnyObject) {
	}
	@IBAction func saveBtn(sender: AnyObject) {
		let alertController = UIAlertController(title: "Savar Gráfico", message:
				"Deseja salvar o gráfico?", preferredStyle: UIAlertControllerStyle.Alert)
		alertController.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.Default, handler: nil))

		self.presentViewController(alertController, animated: true, completion: nil)
		graficoView.saveToCameraRoll()
	}

	func limpaGrafico() {
		if self.currentSubLayer != nil {
			self.currentSubLayer.removeFromSuperlayer()
		}
		floatingGraphReader.hidden = true
	}
}
