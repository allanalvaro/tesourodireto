//
//  MaterialTextField.swift
//  myposts
//
//  Created by Allan Alvaro Gomes on 05/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class CustomMaterialLabel: MaterialLabel {

	override func awakeFromNib() {
		layer.cornerRadius = 3.0
		layer.shadowColor = UIColor(white: SHADOW_COLOR, alpha: 0.1).CGColor
		layer.borderWidth = 0.0
	}

	// For Placeholder
	func textRectForBounds(bounds: CGRect) -> CGRect {
		return CGRectInset(bounds, 10, 0)
	}

	// For editable text
	func editingRectForBounds(bounds: CGRect) -> CGRect {
		return CGRectInset(bounds, 10, 0)
	}
}
