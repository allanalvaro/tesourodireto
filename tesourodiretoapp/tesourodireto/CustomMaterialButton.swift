//
//  MaterialButton.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 13/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class CustomMaterialButton: MaterialButton {

	override func awakeFromNib() {
		self.cornerRadius = 3.0
//        self.shadowColor = UIColor(white: SHADOW_COLOR, alpha: 0.0)
//        self.shadowOpacity = 0.8
//        self.shadowRadius = 5.0
//        self.shadowOffset = CGSizeMake(0.0, 2.0)
		self.backgroundColor = UIColor(white: SHADOW_COLOR, alpha: 0.1)
		self.borderWidth = 1.0
	}
}
