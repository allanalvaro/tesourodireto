//
//  MaterialView.swift
//
//
//  Created by Allan Alvaro Gomes on 05/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class CustomMaterialView: MaterialView {

	override func awakeFromNib() {
		layer.cornerRadius = 3.0
		// layer.shadowColor = UIColor(white: SHADOW_COLOR, alpha: 0.5).CGColor
		// layer.shadowOpacity = 0.8
		// layer.shadowRadius = 5.0
		// layer.shadowOffset = CGSizeMake(0.0, 2.0)
		layer.backgroundColor = UIColor(white: SHADOW_COLOR, alpha: 0.1).CGColor
	}
}
