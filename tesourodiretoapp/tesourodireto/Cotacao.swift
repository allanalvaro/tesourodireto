//
//  Cotacao.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 17/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation

class Cotacao {
	private var _data: String!
	private var _tCompra = "-"
	private var _tVenda = "-"
	private var _pCompra = "-"
	private var _pVenda = "-"

	var data: String {
		if self._data == nil {
			return ""
		}
		return self._data
	}
	var tCompra: String {
		return self._tCompra
	}
	var tVenda: String {
		return self._tVenda
	}
	var pCompra: String {
		return self._pCompra
	}
	var pVenda: String {
		return self._pVenda
	}

	init(data: String, tCompra: String, tVenda: String, pCompra: String, pVenda: String) {

		self._data = data
		self._tCompra = tCompra
		self._tVenda = tVenda
		self._pCompra = pCompra
		self._pVenda = pVenda
	}
}