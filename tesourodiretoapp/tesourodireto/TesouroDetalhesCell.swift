//
//  TesouroDetalhesCell.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 20/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class TesouroDetalhesCell: MaterialTableViewCell {

	@IBOutlet weak var dataLbl: UILabel!
	@IBOutlet weak var taxaVendaLbl: UILabel!
	@IBOutlet weak var precoVendaLbl: UILabel!
	@IBOutlet weak var taxaCompraLbl: UILabel!
	@IBOutlet weak var precoCompraLbl: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
	}

	func configureCell(data: String, tCompra: String, tVenda: String, pCompra: String, pVenda: String) {

		dataLbl.text = "Data: \(data)"

		if tCompra == "-" {
			self.taxaCompraLbl.text = tCompra
		} else {
			self.taxaCompraLbl.text = "\(tCompra)%"
		}
		if tVenda == "-" {
			self.taxaVendaLbl.text = tVenda
		} else {
			self.taxaVendaLbl.text = "\(tVenda)%"
		}
		if pVenda == "-" {
			self.precoVendaLbl.text = pVenda
		} else {
			self.precoVendaLbl.text = "R$ \(pVenda)"
		}
		if pCompra == "-" {
			self.precoCompraLbl.text = pCompra
		} else {
			self.precoCompraLbl.text = "R$ \(pCompra)"
		}
	}
}
