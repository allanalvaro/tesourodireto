//
//  SelicCell.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 26/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit

class SelicCell: UITableViewCell {

	@IBOutlet weak var dataLbl: UILabel!
	@IBOutlet weak var taxaLbl: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
	}

	func configureCell(data: String, taxa: String) {

		dataLbl.text = "Vigência: \(data)"
		taxaLbl.text = "\(taxa)% a.a"
	}
}