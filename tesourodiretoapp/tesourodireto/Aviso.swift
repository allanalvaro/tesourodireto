//
//  Aviso.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 31/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation

class Aviso {

	private var _titulo: String
	private var _mensagem: String
	private var _dia: String
	private var _mesNome: String
	private var _ano: String
	private var _mes: String

	var titulo: String {
		return _titulo
	}
	var mensagem: String {
		return _mensagem
	}
	var dia: String {
		return _dia
	}
	var mesNome: String {
		return _mesNome
	}
	var ano: String {
		return _ano
	}
	var mes: String {
		return _mes
	}

	init(titulo: String, mensagem: String, dia: String, mesNome: String, ano: String, mes: String) {
		self._mensagem = mensagem
		self._dia = dia
		self._mesNome = mesNome
		self._ano = ano
		self._titulo = titulo
		self._mes = mes
	}
}