//
//  TesouroDetalhesViewController.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 19/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Firebase
import Charts
import Google

class TesouroDetalhesViewController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource, ChartViewDelegate {

	@IBOutlet weak var tituloNomeLbl: UILabel!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var segmentControl: ADVSegmentedControl!
	@IBOutlet weak var graficoView: LineChartView!
	@IBOutlet weak var floatingGraphReader: FloatingGraphReader!

	var titulo: Titulo!
	var titulosCotacoes: [Titulo]!
	var graficoDatas: [String]!
	var graficoCotacoes: [Double]!
	var currentSubLayer: CAShapeLayer!
	// var gradientColors = [ChartColorTemplates.colorFromString("#00ff0000").CGColor, ChartColorTemplates.colorFromString("#ffff0000").CGColor]

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(true)
		self.screenName = "Tesouro Detalhes View Controller"
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		titulosCotacoes = []

		tableView.delegate = self
		tableView.dataSource = self

		floatingGraphReader.hidden = true

		graficoView.delegate = self
		graficoView.descriptionText = ""
		graficoView.noDataText = "Carregando dados..."
		graficoView.descriptionTextColor = UIColor.grayColor()

		initObeservers(7)

		segmentControl.items = ["1 sem", "2 sem ", "1 m", "3 m", "6 m"]
		segmentControl.font = UIFont(name: "Avenir-Black", size: 11)
		segmentControl.borderColor = UIColor(white: 1.0, alpha: 0.3)
		segmentControl.selectedIndex = 0
		segmentControl.alpha = 0.5
		segmentControl.addTarget(self, action: #selector(TesouroDetalhesViewController.segmentValueChanged(_:)), forControlEvents: .ValueChanged)
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return titulosCotacoes.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let cell = tableView.dequeueReusableCellWithIdentifier("TesouroDetalhesCell") as? TesouroDetalhesCell {

			let titulo = self.titulosCotacoes[indexPath.row]

			cell.configureCell(titulo.cotacao.data, tCompra: titulo.cotacao.tCompra, tVenda: titulo.cotacao.tVenda, pCompra: titulo.cotacao.pCompra, pVenda: titulo.cotacao.pVenda)
			return cell
		} else {
			return TesouroDetalhesCell()
		}
	}

	func initObeservers(periodo: Int?) {
		DataService.ds.REF_COTACOES.childByAppendingPath("fechamento").childByAppendingPath(titulo.id).queryLimitedToLast(UInt(periodo!)).observeSingleEventOfType(.Value, withBlock: { snapshot in

			if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {

				self.titulosCotacoes = []
				self.graficoDatas = []
				self.graficoCotacoes = []

				for snap in snapshots {
					var pVenda = "", pCompra = "", tCompra = "", tVenda = "", data = "", nome = ""

					if let tituloDict = snap.value as? Dictionary<String, AnyObject> {
						if let pV = tituloDict["pvenda"] as? String {
							pVenda = pV
						}
						if let pC = tituloDict["pcompra"] as? String {
							pCompra = pC
						}
						if let tV = tituloDict["tvenda"] as? String {
							tVenda = tV
						}
						if let tC = tituloDict["tcompra"] as? String {
							tCompra = tC
						}
						if let tNome = tituloDict["nome"] as? String {
							self.tituloNomeLbl.text = tNome
						}
						if let dt = tituloDict["data"] as? String {
							data = dt
						}

						let titulo = Titulo(nome: nome, vencimento: "", tipo: "", id: "")
						let cotacao = Cotacao(data: data, tCompra: tCompra, tVenda: tVenda, pCompra: pCompra, pVenda: pVenda)
						titulo.adicionaCotacao(cotacao)
						self.titulosCotacoes.append(titulo)

						self.graficoDatas.append(data)
						let taxa = tCompra.replace(",", withString: ".")
						if let cot = (taxa as? NSString)?.doubleValue {
							self.graficoCotacoes.append(cot)
						} else {
							self.graficoCotacoes.append(0.0)
						}
					}
				}
			}
			self.titulosCotacoes = self.titulosCotacoes.reverse()
			self.tableView.reloadData()

			self.setChart(self.graficoDatas, valores: self.graficoCotacoes)
		})
	}

	func segmentValueChanged(sender: AnyObject?) {

		if segmentControl.selectedIndex == 0 {
			initObeservers(7)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 1 {
			initObeservers(14)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 2 {
			initObeservers(31)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 3 {
			initObeservers(92)
			limpaGrafico()
		} else if segmentControl.selectedIndex == 4 {
			initObeservers(185)
			limpaGrafico()
		} else {
			initObeservers(1)
		}
	}

	@IBAction func backButtonTapped(sender: AnyObject) {
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	@IBAction func saveBtn(sender: AnyObject) {
		graficoView.saveToCameraRoll()
	}
	@IBAction func menuBtn(sender: AnyObject) {
	}

	func setChart(dataPoints: [String], valores: [Double]) {

		var dataEntries: [ChartDataEntry] = []

		for i in 0 ..< dataPoints.count {
			let dataEntry = ChartDataEntry(value: valores[i], xIndex: i)
			dataEntries.append(dataEntry)
		}

		let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "Taxa Compra")
		lineChartDataSet.lineWidth = 1.5
		lineChartDataSet.drawCirclesEnabled = true
		lineChartDataSet.circleRadius = 1.7
		// lineChartDataSet.setCircleColor(NSUIColor.yellowColor())
		lineChartDataSet.drawCubicEnabled = true
		lineChartDataSet.drawValuesEnabled = false
		lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
		lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
		lineChartDataSet.drawFilledEnabled = true
		lineChartDataSet.fillAlpha = 0.1

		let lineChartData = LineChartData(xVals: dataPoints, dataSet: lineChartDataSet)
		graficoView.data = lineChartData

		graficoView.leftAxis.valueFormatter = NSNumberFormatter()
		graficoView.rightAxis.valueFormatter = NSNumberFormatter()
		graficoView.leftAxis.valueFormatter?.maximumFractionDigits = 2
		graficoView.rightAxis.valueFormatter?.maximumFractionDigits = 2
		graficoView.leftAxis.valueFormatter?.minimumFractionDigits = 2
		graficoView.rightAxis.valueFormatter?.minimumFractionDigits = 2

		graficoView.rightAxis.gridColor = UIColor.lightGrayColor()
		graficoView.leftAxis.gridColor = UIColor.lightGrayColor()
		graficoView.xAxis.gridColor = UIColor.clearColor()

		graficoView.rightAxis.labelTextColor = UIColor.lightGrayColor()
		graficoView.leftAxis.labelTextColor = UIColor.lightGrayColor()
		graficoView.xAxis.labelTextColor = UIColor.lightGrayColor()

		graficoView.animate(xAxisDuration: 1.0, yAxisDuration: 0.0)
		graficoView.dragEnabled = true
		graficoView.pinchZoomEnabled = true
		graficoView.highlightPerDragEnabled = false
	}

	func chartValueNothingSelected(chartView: ChartViewBase) {
		limpaGrafico()
	}

	func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {

		let markerPosition = graficoView.getMarkerPosition(entry: entry, highlight: highlight)

		createCircle(markerPosition.x, y: markerPosition.y)

		// Adding top marker
		floatingGraphReader.taxaLbl.text = "\(entry.value)%"
		floatingGraphReader.dataLbl.text = "\(self.graficoDatas[entry.xIndex])"
		floatingGraphReader.center = CGPointMake(markerPosition.x, markerPosition.y + 20)
		floatingGraphReader.hidden = false
	}

	func createCircle(x: CGFloat, y: CGFloat) {
		let circlePath = UIBezierPath(arcCenter: CGPoint(x: x, y: y), radius: CGFloat(2), startAngle: CGFloat(0), endAngle: CGFloat(M_PI * 2), clockwise: true)

		let shapeLayer = CAShapeLayer()
		shapeLayer.path = circlePath.CGPath

		// change the fill color
		shapeLayer.fillColor = UIColor.clearColor().CGColor
		// you can change the stroke color
		shapeLayer.strokeColor = UIColor.redColor().CGColor
		// you can change the line width
		shapeLayer.lineWidth = 1.5

		if self.currentSubLayer != nil {
			self.currentSubLayer.removeFromSuperlayer()
		}
		graficoView.layer.addSublayer(shapeLayer)
		self.currentSubLayer = shapeLayer
	}

	func limpaGrafico() {
		if self.currentSubLayer != nil {
			self.currentSubLayer.removeFromSuperlayer()
		}
		floatingGraphReader.hidden = true
	}
}
