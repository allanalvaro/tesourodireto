//
//  DataServices.swift
//  myposts
//
//  Created by Allan Alvaro Gomes on 17/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation
import Firebase

let URL_BASE = "https://tesourodireto.firebaseio.com"
class DataService {
	static let ds = DataService()

	private var _REF_BASE = Firebase(url: "\(URL_BASE)")
	private var _REF_SELIC = Firebase(url: "\(URL_BASE)/selic")
	private var _REF_LAST_GET = Firebase(url: "\(URL_BASE)/lastget")
	private var _REF_ULTIMO_PERIODO = Firebase(url: "\(URL_BASE)/ultimoperiodo")
	private var _REF_COTACOES = Firebase(url: "\(URL_BASE)/cotacoes")
	private var _REF_IDS = Firebase(url: "\(URL_BASE)/ids")
	private var _REF_TITULOS = Firebase(url: "\(URL_BASE)/titulos")
	private var _REF_ULTIMA_COTACAO = Firebase(url: "\(URL_BASE)/ultimacotacao")
	private var _REF_AVISOS = Firebase(url: "\(URL_BASE)/avisos")

	var REF_BASE: Firebase {
		return _REF_BASE
	}
	var REF_IDS: Firebase {
		return _REF_IDS
	}
	var REF_LAST_GET: Firebase {
		return _REF_LAST_GET
	}
	var REF_TITULOS: Firebase {
		return _REF_TITULOS
	}
	var REF_ULTIMO_PERIODO: Firebase {
		return _REF_ULTIMO_PERIODO
	}
	var REF_ULTIMA_COTACAO: Firebase {
		return _REF_ULTIMA_COTACAO
	}
	var REF_COTACOES: Firebase {
		return _REF_COTACOES
	}
	var REF_SELIC: Firebase {
		return _REF_SELIC
	}
	var REF_AVISOS: Firebase {
		return _REF_AVISOS
	}

//    var REF_USER_CURRENT: Firebase {
//        // if ap is deleted and user try to login with a user that exists, uid will receive nil
//        let uid = NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) as! String
//        let user = Firebase(url: "\(URL_BASE)").childByAppendingPath("users").childByAppendingPath(uid)
//        return user!
//    }
}
