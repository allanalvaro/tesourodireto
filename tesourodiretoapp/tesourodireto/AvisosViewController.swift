//
//  AvisosViewController.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 16/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Firebase
import Google

class AvisosViewController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!

	var avisosArray: [Aviso]!

	override func viewDidLoad() {
		super.viewDidLoad()

		avisosArray = []

		initObeservers()

		tableView.delegate = self
		tableView.dataSource = self

		self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(true)
		self.screenName = "Avisos View Controller"
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let cell = tableView.dequeueReusableCellWithIdentifier("AvisosCell") as? AvisosCell {

			let aviso = self.avisosArray[indexPath.row]

			cell.configureCell(aviso.titulo, mensagem: aviso.mensagem, dia: aviso.dia, mesNome: aviso.mesNome, ano: aviso.ano)
			return cell
		} else {
			return AvisosCell()
		}
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return avisosArray.count
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func initObeservers() {
		DataService.ds.REF_AVISOS.observeEventType(.Value, withBlock: { snapshot in

			if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {

				self.avisosArray = []

				for snap in snapshots {
					var ano = "", dia = "", mensagem = "", mes = "", mesNome = "", titulo = ""

					if let avisosDict = snap.value as? Dictionary<String, AnyObject> {
						if let _ano = avisosDict["ano"] as? String {
							ano = _ano
						}
						if let _dia = avisosDict["dia"] as? String {
							dia = _dia
						}
						if let _mensagem = avisosDict["mensagem"] as? String {
							mensagem = _mensagem
						}
						if let _mes = avisosDict["mes"] as? String {
							mes = _mes
						}
						if let _mesNome = avisosDict["mesnome"] as? String {
							mesNome = _mesNome
						}
						if let _titulo = avisosDict["titulo"] as? String {
							titulo = _titulo
						}

						let aviso = Aviso(titulo: titulo, mensagem: mensagem, dia: dia, mesNome: mesNome, ano: ano, mes: mes)

						self.avisosArray.append(aviso)
					}
				}
			}
			self.avisosArray = self.avisosArray.reverse()
			self.tableView.reloadData()
		})
	}
}
