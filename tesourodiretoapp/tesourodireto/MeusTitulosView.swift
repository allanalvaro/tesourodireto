//
//  MeusTitulosView.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 27/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit

class MeusTitulosView: UIView, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!

	var titulosArray = [MeusTitulosCell]()

	override func awakeFromNib() {
		super.awakeFromNib()

		self.tableView.delegate = self
		self.tableView.dataSource = self
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return titulosArray.count
	}

	func tableView(tableVießw: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		return UITableViewCell()
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
}
