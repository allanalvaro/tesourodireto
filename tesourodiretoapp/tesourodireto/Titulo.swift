//
//  Tesouro.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 17/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation
import Firebase

class Titulo {

	private var _id: String!
	private var _nome: String!
	private var _vencimento: String!
	private var _tipo: String!
	private var _cotacao: Cotacao!

	var id: String {
		if self._id == nil {
			return ""
		}
		return self._id
	}
	var nome: String {
		if self._nome == nil {
			return ""
		}
		return self._nome
	}
	var vencimento: String {
		if self._vencimento == nil {
			return ""
		}
		return self._vencimento
	}
	var tipo: String {
		if self._tipo == nil {
			return ""
		}
		return self._tipo
	}
	var cotacao: Cotacao {
		return self._cotacao
	}

	init(nome: String, vencimento: String, tipo: String, id: String) {
		self._nome = nome
		self._tipo = tipo
		self._vencimento = vencimento
		self._id = id
	}
	func adicionaCotacao(cot: Cotacao) {
		self._cotacao = cot
	}
}
