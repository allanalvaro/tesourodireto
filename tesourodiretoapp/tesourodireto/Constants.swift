//
//  Constants.swift
//  myposts
//
//  Created by Allan Alvaro Gomes on 05/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation
import UIKit
import Google

let SHADOW_COLOR: CGFloat = 157.0 / 255.0

//Keys
let KEY_UID = "uid"

//segues
let LOGGED_IN = "Loggedin"

//Status code
let STATUS_ACCOUNT_NONEXIST = -8
let STATUS_MISSING_CREDENTS = -5

extension String
{
	func replace(target: String, withString: String) -> String
	{
		return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
	}
}

func analyticsSendEvent(name: String, action: String, label: String) {
	let tracker = GAI.sharedInstance().defaultTracker

	tracker.send(GAIDictionaryBuilder.createEventWithCategory(name, action: action, label: label, value: nil).build() as [NSObject: AnyObject])
}