//
//  Selic.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 26/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import Foundation

class Selic {
	private var _vigencia: String!
	private var _taxa: String!

	var vigencia: String {
		return self._vigencia
	}
	var taxa: String {
		return self._taxa
	}

	init(vigencia: String, taxa: String) {

		self._vigencia = vigencia
		self._taxa = taxa
	}
}