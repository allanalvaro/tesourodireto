//
//  AvisosCell.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 26/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit

class AvisosCell: UITableViewCell {

	@IBOutlet weak var tituloLbl: UILabel!
	@IBOutlet weak var mensagemLbl: UILabel!
	@IBOutlet weak var diaLbl: UILabel!
	@IBOutlet weak var mesNomeLbl: UILabel!
	@IBOutlet weak var anoLbl: UILabel!
	@IBOutlet weak var dataView: UIView!

	override func awakeFromNib() {
		super.awakeFromNib()

		dataView.layer.cornerRadius = 4.0
	}

	func configureCell(titulo: String, mensagem: String, dia: String, mesNome: String, ano: String) {
		tituloLbl.text = titulo
		mensagemLbl.text = mensagem
		diaLbl.text = dia
		mesNomeLbl.text = mesNome
		anoLbl.text = ano
	}
}