//
//  ViewController.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 15/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import SWRevealViewController
import Firebase
import Google

class TesouroViewController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var ultimaAutalizacaoLbl: UILabel!
	@IBOutlet weak var segmentControl: ADVSegmentedControl!
	@IBOutlet weak var MeusTitulosView: UIView!

	var nomesTitulosArray = [String]()
	var titulos = [Titulo]()

	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		tableView.rowHeight = UITableViewAutomaticDimension
		self.navigationController?.navigationBarHidden = true
		self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

		initObeservers()

		segmentControl.items = ["Meus Títulos", "Todos os Títulos"]
		segmentControl.font = UIFont(name: "Avenir-Black", size: 10)
		segmentControl.borderColor = UIColor(white: 1.0, alpha: 0.3)
		segmentControl.selectedIndex = 0
		segmentControl.alpha = 0.5
		segmentControl.addTarget(self, action: "segmentValueChanged:", forControlEvents: .ValueChanged)
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(true)
		self.screenName = "Tesouro View Controller"
	}

	func initObeservers() {

		DataService.ds.REF_ULTIMA_COTACAO.observeEventType(.Value, withBlock: { snapshot in

			self.ultimaAutalizacaoLbl.text = "Atualizado em:"
			if let data = snapshot.value["timeultimacotacao"] as? String! {
				self.ultimaAutalizacaoLbl.text = "Atualizado em: \(data)"
			}

			if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {
				self.titulos = []

				for snap in snapshots {
					var id = "", vencimento = "", nome = "", tipo = "", periodo = "", hora = ""
					var pVenda = "", pCompra = "", tCompra = "", tVenda = ""

					if let tituloDict = snap.value as? Dictionary<String, AnyObject> {
						if let pV = tituloDict["pvenda"] as? String {
							pVenda = pV
						}
						if let pC = tituloDict["pcompra"] as? String {
							pCompra = pC
						}
						if let tV = tituloDict["tvenda"] as? String {
							tVenda = tV
						}
						if let tC = tituloDict["tcompra"] as? String {
							tCompra = tC
						}
						if let tNome = tituloDict["nome"] as? String {
							nome = tNome
						}
						if let pe = tituloDict["periodo"] as? String {
							periodo = pe
						}
						if let tHora = tituloDict["hora"] as? String {
							hora = tHora
						}
						if let tId = tituloDict["id"] as? String {
							id = tId
						}
						if let tVenc = tituloDict["vencimento"] as? String {
							vencimento = tVenc
						}
						if let tTipo = tituloDict["tipo"] as? String {
							tipo = tTipo
						}

						let titulo = Titulo(nome: nome, vencimento: vencimento, tipo: tipo, id: id)
						let cotacao = Cotacao(data: "", tCompra: tCompra, tVenda: tVenda, pCompra: pCompra, pVenda: pVenda)
						titulo.adicionaCotacao(cotacao)
						self.titulos.append(titulo)
					}
				}
			}
			self.titulos = self.titulos.reverse()
			self.tableView.reloadData()
		})
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return titulos.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let cell = tableView.dequeueReusableCellWithIdentifier("TesouroCell") as? TesouroCell {

			let titulo = self.titulos[indexPath.row]
			// print(titulo.nome)

			cell.configureCell(titulo.nome, tCompra: titulo.cotacao.tCompra, tVenda: titulo.cotacao.tVenda, pCompra: titulo.cotacao.pCompra, pVenda: titulo.cotacao.pVenda)

			return cell
		} else {
			return TesouroCell()
		}
	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		if self.titulos[indexPath.row].cotacao.pCompra == "-" && self.titulos[indexPath.row].cotacao.tCompra == "-" {
			return 48.0
		} else {
			return 75.0
		}
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let tit = self.titulos[indexPath.row]

		analyticsSendEvent(tit.nome, action: "Titulo Selecionado", label: "Titulos")

		performSegueWithIdentifier("tesourodetalhesviewcontroller", sender: tit)
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "tesourodetalhesviewcontroller" {
			if let tesouroDetalhesVC = segue.destinationViewController as? TesouroDetalhesViewController {
				if let tit = sender as? Titulo {
					tesouroDetalhesVC.titulo = tit
				}
			}
		}
	}

	func segmentValueChanged(sender: AnyObject?) {
		if segmentControl.selectedIndex == 0 {
			self.MeusTitulosView.hidden = false
		} else if segmentControl.selectedIndex == 1 {
			self.MeusTitulosView.hidden = true
		} else {
		}
	}
}
