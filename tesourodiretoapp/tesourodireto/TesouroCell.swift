//
//  TesouroCell.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 16/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class TesouroCell:  MaterialTableViewCell {
    
    @IBOutlet weak var tesouroNomeLbl: UILabel!
    @IBOutlet weak var taxaCompraLbl: UILabel!
    @IBOutlet weak var taxaVendaLbl: UILabel!
    @IBOutlet weak var precoCompraLbl: UILabel!
    @IBOutlet weak var precoVendaLbl: UILabel!
    @IBOutlet weak var compraLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(nome: String, tCompra: String, tVenda: String, pCompra: String, pVenda: String) {
        
        self.tesouroNomeLbl.text = nome
        if tCompra == "-" {
            self.taxaCompraLbl.text = tCompra
        } else {
            self.taxaCompraLbl.text = "\(tCompra)%"
        }
        if tVenda == "-" {
            self.taxaVendaLbl.text = tVenda
        } else {
            self.taxaVendaLbl.text = "\(tVenda)%"
        }
        if pVenda == "-" {
            self.precoVendaLbl.text = pVenda
        } else {
            self.precoVendaLbl.text = "R$ \(pVenda)"
        }
        if pCompra == "-" {
            self.precoCompraLbl.text = pCompra
        } else {
            self.precoCompraLbl.text = "R$ \(pCompra)"
        }
        if pCompra == "-" && tCompra == "-" {
            taxaCompraLbl.hidden = true
            precoCompraLbl.hidden = true
            compraLbl.hidden = true
        } else {
            taxaCompraLbl.hidden = false
            precoCompraLbl.hidden = false
            compraLbl.hidden = false
        }
    }
}
