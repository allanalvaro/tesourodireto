//
//  MenuTableViewController.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 16/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit
import Material

class MenuTableViewController: UITableViewController {

	var menuArray = [String]()

	override func viewDidLoad() {
		super.viewDidLoad()

		menuArray = ["Menu", "tesouro", "selic", "avisostesouro"]
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let cell = tableView.dequeueReusableCellWithIdentifier(menuArray[indexPath.row]) as? RearMenuCell {

			return cell
		}
		return RearMenuCell()
	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return menuArray.count
	}
}
