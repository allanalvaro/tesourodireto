//
//  MenuCell.swift
//  tesourodireto
//
//  Created by Allan Alvaro Gomes on 15/03/16.
//  Copyright © 2016 allan. All rights reserved.
//

import UIKit

class RearMenuCell: UITableViewCell {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.selectedBackgroundView?.backgroundColor = UIColor.lightGrayColor()
	}
}
