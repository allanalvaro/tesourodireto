#!/bin/bash

CONSULTA_LINK='www.tesouro.gov.br/web/stn/avisos'
TIMESTAMP=$(date +'%d%m%y%H%m%s')
LOG="logs/avisos/avisos-$TIMESTAMP.log"
#PYTHON='/Library/Frameworks/Python.framework/Versions/3.4/bin/python3'
PYTHON='/usr/bin/python3'
DUMP=".avisosdump"
TMPDUMP="/tmp/.avisosdump"

curl -s $CONSULTA_LINK > $TMPDUMP
diff $TMPDUMP $DUMP
if [[ $? != 0 ]]; then
  echo "Atualizacao para a avisos" >> $LOG
  echo "Executando: parsehub-avisos.py..." >> $LOG
  $PYTHON parsehub-avisos.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando o parsehub-avisos.py" >> $LOG
    echo "Fazendo nova tentativa em 1 minuto..." >> $LOG
    sleep 60
    $PYTHON parsehub-avisos.py >> $LOG 2>&1
    if [[ $? -ne 0 ]]; then
      echo "Ainda com problemas executando o parsehub-avisos.py" >> $LOG
      echo "Abortando ate a proxima execucao" >> $LOG
      #cat $LOG | slacktee/slacktee.sh
      exit 2
    fi
  fi
  echo "Executando: avisosparser.py..." >> $LOG
  $PYTHON avisosparser.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando avisosparser.py" >> $LOG
    echo "Verificar com URGENCIA!!" >> $LOG
    #cat $LOG | slacktee/slacktee.sh
    exit 3
  fi
else
  echo "Sem atualizacao para a avisos"
  rm -f $TMPDUMP
  exit 0
fi

mv -f $TMPDUMP $DUMP
#echo "Execucao avisos OK" | slacktee/slacktee.sh
