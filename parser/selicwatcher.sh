#!/bin/bash

CONSULTA_LINK='http://www.bcb.gov.br/Pec/Copom/Port/taxaSelic.asp'
TIMESTAMP=$(date +'%d%m%y%H%m%s')
LOG="logs/selic/selic-$TIMESTAMP.log"
#PYTHON='/Library/Frameworks/Python.framework/Versions/3.4/bin/python3'
PYTHON='/usr/bin/python3'
DUMP=".selicdump"
TMPDUMP="/tmp/.selicdump"

curl -s $CONSULTA_LINK > $TMPDUMP
diff $TMPDUMP $DUMP
if [[ $? != 0 ]]; then
  echo "Atualizacao para a Selic" >> $LOG
  echo "Executando: parsehub-selic.py..." >> $LOG
  $PYTHON parsehub-selic.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando o parsehub-selic.py" >> $LOG
    echo "Fazendo nova tentativa em 1 minuto..." >> $LOG
    sleep 60
    $PYTHON parsehub-selic.py >> $LOG 2>&1
    if [[ $? -ne 0 ]]; then
      echo "Ainda com problemas executando o parsehub-selic.py" >> $LOG
      echo "Abortando ate a proxima execucao" >> $LOG
      cat $LOG | slacktee/slacktee.sh
      exit 2
    fi
  fi
  echo "Executando: selicparser.py..." >> $LOG
  $PYTHON selicparser.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando selicparser.py" >> $LOG
    echo "Verificar com URGENCIA!!" >> $LOG
    cat $LOG | slacktee/slacktee.sh
    exit 3
  fi
else
  echo "Sem atualizacao para a Selic"
  rm -f $TMPDUMP
  exit 0
fi

mv -f $TMPDUMP $DUMP
echo "Execucao Selic OK" | slacktee/slacktee.sh
