# !/usr/bin/env python3

import requests
from firebase import firebase
from subprocess import check_call
import json
import re
import time
import gzip
import shutil
import os

url_cotacoes = 'https://tesourodireto.firebaseio.com/cotacoes/fechamento'
urlend = '.json?auth=3ACaIBDlNv38no10JV5hMjjs4JV6C5qxggQCwvv9'
inputfile = 'LOAD.json'

tNome='Tesouro IGPM+ com Juros Semestrais 2031 (NTNC)'

def load(tNome=None):
    t_id = re.sub('[^A-Za-z0-9]+', '', tNome)

    with open(inputfile) as data_file:
        cotacoes = json.load(data_file)

    for cotacao in cotacoes:
        data = cotacao['Dia']
        pCompra = cotacao['pCompra']
        pVenda = cotacao['pVenda']
        tCompra = cotacao['tCompra']
        tVenda = cotacao['tVenda']

        data = re.sub('/', '-', data)

        tCompra = re.sub('%', '', tCompra)
        tVenda = re.sub('%', '', tVenda)

        #pCompra = re.sub('\.', ',', str(pCompra))
        #pVenda = re.sub('\.', ',', str(pVenda))
        mes = data.split(':')[1]
        dia = data.split(':')[0]
        ano = data.split(':')[2]
        data = ano + mes + dia 

        url = url_cotacoes + '/' + t_id + '/' + data
        payload = {'nome': tNome, 'data': data, 'pcompra': pCompra, 'pvenda': pVenda, 'tcompra': tCompra, 'tvenda': tVenda}
        payload = json.dumps(payload)
        load = requests.patch(url + urlend, data=payload)





load(tNome)
