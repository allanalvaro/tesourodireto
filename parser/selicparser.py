# !/usr/bin/env python3

import requests
import json
import re

url = 'https://tesourodireto.firebaseio.com/selic/'
urlend = '.json?auth=3ACaIBDlNv38no10JV5hMjjs4JV6C5qxggQCwvv9'

inputfile = 'selicparser.json'

def acertaData(data=None):
    mes = data.split('/')[1]
    dia = data.split('/')[0]
    ano = data.split('/')[2]
    data = ano + '-' + mes + '-' + dia
    return data

def carregaSelic(primeiraData=None, ano=None, dataCheia=None):
    payload = {'data': primeiraData, 'ano': ano, 'datacheia': dataCheia}
    payload = json.dumps(payload)
    load = requests.patch(url + primeiraData + '/' + urlend, data=payload)

# load json file from parsehub
with open(inputfile) as data_file:
    vigencias = json.load(data_file)

for vigencia in vigencias['vigencia']:
    primeiraData = vigencia['data'].split('-')[0]
    primeiraData = acertaData(primeiraData)
    primeiraData = re.sub(' ', '', primeiraData)

    carregaSelic(primeiraData, vigencia['ano'], vigencia['data'])
