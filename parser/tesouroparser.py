# !/usr/bin/env python3

import requests
from firebase import firebase
from subprocess import check_call
import json
import re
import time
import gzip
import shutil
import os

url = 'https://tesourodireto.firebaseio.com/'
url_titulos = 'https://tesourodireto.firebaseio.com/titulos/'
url_id = 'https://tesourodireto.firebaseio.com/ids/'
url_cotacoes = 'https://tesourodireto.firebaseio.com/cotacoes/'
url_last = 'https://tesourodireto.firebaseio.com/ultimacotacao/'
urlend = '.json?auth=3ACaIBDlNv38no10JV5hMjjs4JV6C5qxggQCwvv9'
fechamento = 'fechamento'
tarde = 'tarde'
manha = 'manha'
t_igpm = 'Indexados ao IGP-M'
t_ipca = 'Indexados ao IPCA'
t_pref = 'Prefixados'
t_selic = 'Indexados a Taxa Selic'
inputfile = 'tesouroparser.json'

def fbBackup(data=None):
    file = 'backups/fbbackup-' + data + '.json'
    print('Fazendo backup do dia anterior: ' + file)
    load = requests.get(url + urlend)
    b = open(file, 'w')
    b.write(load.text)
    b.close
    print('Compactando...')
    with open(file, 'rb') as f_in, gzip.open(file + '.gz', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)
    os.system('rm -f ' + file)

def checaTipo(name=None):
    if 'Tesouro IGPM' in (name):
        return t_igpm
    elif 'Tesouro IPCA' in (name):
        return t_ipca
    elif 'Tesouro Prefixado' in (name):
        return t_pref
    else:
        return t_selic

def acertaData(data=None):
    mes = data.split('-')[1]
    dia = data.split('-')[0]
    ano = data.split('-')[2]
    data = ano + '-' + mes + '-' + dia
    return data

def criaTituloId(nome=None, _id=None):
    payload = {nome: _id}
    payload = json.dumps(payload)
    load = requests.patch(url_id + urlend, data=payload)

def criaTitulo(url=None, nome=None, tipo=None, vencimento=None, _id=None):
    payload = {'nome': nome, 'tipo': tipo, 'vencimento': vencimento, 'id': _id}
    print(nome)
    #print json.dumps(payload, indent=4)
    payload = json.dumps(payload)
    load = requests.patch(url + urlend, data=payload)
    #print load
    criaTituloId(nome, _id)

def criaPeriodo(url=None, pc=None, pv=None, tc=None, tv=None, pe=None, nome=None, data=None):
    payload = {'pcompra': pc, 'pvenda': pv, 'tcompra': tc, 'tvenda': tv, 'nome': nome, 'data': data}
    #print('criando periodo: ' + pe)
    #print json.dumps(payload, indent=4)
    payload = json.dumps(payload)
    load = requests.patch(url + urlend, data=payload)
    #print load

def atualizaLastGet(lastget):
    payload = {'lastget': lastget}
    print('Atualizando lastget para: ' + lastget)
    #print json.dumps(payload, indent=4)
    payload = json.dumps(payload)
    load = requests.patch(url + urlend, data=payload)
    #print load

def atualizaPeriodo(periodo=None):
    payload = {'ultimoperiodo': periodo}
    #print('Atualizando periodo para: ' + periodo)
    #print json.dumps(payload, indent=4)
    payload = json.dumps(payload)
    load = requests.patch(url + urlend, data=payload)
    #print load

def proximoPeriodo(ultimoperiodo=None):
    if ultimoperiodo == fechamento:
        return manha
    elif ultimoperiodo == tarde:
        return fechamento
    else:
        return tarde

def atualizaUltimaCotacao(url=None, _id=None, nome=None, pcompra=None, pvenda=None, tvenda=None, tcompra=None, time=None, pe=None, vencimento=None, lastget=None):
    payload = {'pcompra': pcompra, 'pvenda': pvenda, 'tvenda': tvenda, 'tcompra': tcompra, 'nome': nome, 'hora': time, 'periodo': pe, 'id': _id, 'vencimento': vencimento}
    payload = json.dumps(payload)
    load = requests.patch(url + '/' + _id + urlend, data=payload)
    payload = {'timeultimacotacao': lastget}
    payload = json.dumps(payload)
    load = requests.patch(url + urlend, data=payload)

def carregaDados(pe=None, vazio=None):
    for titulo in titulos['titulos']:
        if 'nome' not in titulo:
            continue

        ph_nome = titulo['nome']
        ph_vencimento = titulo['vencimento']

        if vazio == 'VAZIO':
            ph_tcompra = '-'
            ph_tvenda = '-'
            ph_pcompra = '-'
            ph_pvenda = '-'
        else:
            ph_tcompra = titulo['tcompra']
            ph_tvenda = titulo['tvenda']
            ph_pcompra = titulo['pcompra']
            ph_pvenda = titulo['pvenda']

        t_id = re.sub('[^A-Za-z0-9]+', '', ph_nome)

        t_tipo = checaTipo(ph_nome)
        titulosFullUrl = url_titulos + t_id

        criaTitulo(titulosFullUrl, ph_nome, t_tipo, ph_vencimento, t_id)

        #fbloadurl = titulosFullUrl + '/cotacoes/data/' + ph_data + '/' + pe
        #fbloadurl = url_cotacoes + '/' + pe + '/' + ph_data + '/' + t_id
        fbloadurl = url_cotacoes + '/' + pe + '/' + t_id + '/' + ph_data
        criaPeriodo(fbloadurl, ph_pcompra, ph_pvenda, ph_tcompra, ph_tvenda, pe, ph_nome, ph_data)
        #print('Criado periodo para: ' + pe)
        atualizaUltimaCotacao(url_last, t_id, ph_nome, ph_pcompra, ph_pvenda, ph_tvenda, ph_tcompra, ph_horario, pe, ph_vencimento, ph_lastget)

# load json file from parsehub
with open(inputfile) as data_file:
    titulos = json.load(data_file)
# load json from firebase
r = requests.get(url + urlend)
parsed_json = r.json()

# load latest date and time from parsehub
ph_lastget = titulos['horario']
ph_data = ph_lastget.split(' ')[0]
ph_data = acertaData(ph_data)
ph_horario = ph_lastget.split(' ')[1]
ph_lastget = ph_data + ' ' + ph_horario
# load latest date and time from firebase
fb_ultimoperiodo = parsed_json['ultimoperiodo']
fb_lastget = parsed_json['lastget']
fb_data = fb_lastget.split(' ')[0]
fb_horario = fb_lastget.split(' ')[1]

print('Firebase:')
print('ultimoperiodo: ' + fb_ultimoperiodo)
print('lastget ' + fb_lastget)
print('ParseHub: ')
print('lastget ' + ph_lastget)

# Dias diferentes entre FB e PH
# Tambem entra se tiver manha e tarde sem dados, com dados para o fechamento.
if ph_data != fb_data:
    fbBackup(fb_data)
    print('Dias diferentes, fazendo atualizacao: ' + ph_data + '<->' + fb_data)
    if fb_ultimoperiodo == fechamento:
        if ph_horario.split(':')[0] > '13' and ph_horario.split(':')[0] < '17':
            print('Manha vazia e tarde com dados.')
            carregaDados(manha, 'VAZIO')
            atualizaPeriodo(manha)
            print('Atualizado periodo para: ' + manha)
            carregaDados(tarde, '')
            atualizaLastGet(ph_lastget)
        elif ph_horario.split(':')[0] >= '17' and ph_horario.split(':')[0] <= '23':
            print('Manha e tarde vazia e fechamento com dados.')
            carregaDados(manha, 'VAZIO')
            atualizaPeriodo(manha)
            print('Atualizado periodo para: ' + manha)
            carregaDados(tarde, 'VAZIO')
            atualizaPeriodo(tarde)
            print('Atualizado periodo para: ' + tarde)
            carregaDados(fechamento, '')
            atualizaPeriodo(fechamento)
            print('Atualizado periodo para: ' + fechamento)
            atualizaLastGet(ph_lastget)
        else:
            print('Carregando dados da manha.')
            carregaDados(manha, '')
            atualizaPeriodo(manha)
            print('Atualizado periodo para: ' + manha)
            atualizaLastGet(ph_lastget)
    else:
        print('Algum problema com dias diferentes e ultimoperiodo.')
        print(fb_ultimoperiodo + ' <-> ' + fechamento)
        print('Talvez o dia anterior teve menos que 3 cotacoes.')

# Dias iguais, atualizando periodo do dia
elif int(ph_horario.split(':')[0]) > int(fb_horario.split(':')[0]):

    if int(ph_horario.split(':')[0]) <= int('12') and fb_ultimoperiodo == manha:
        print('Nova cotacao para o periodo da manha. Fazendo atualizacao.')
        carregaDados(manha, '')

    if int(ph_horario.split(':')[0]) < int('17') and fb_ultimoperiodo == tarde:
        print('Nova cotacao para o periodo da tarde. Fazendo atualizacao.')
        carregaDados(tarde, '')

    # Caso tenho manha e fechamento com dados e tarde sem dados
    if int(ph_horario.split(':')[0]) >= int('17') and fb_ultimoperiodo == manha:
        print('Periodo da tarde vazio. Preenxendo com -')
        carregaDados(tarde, 'VAZIO')
        atualizaPeriodo(tarde)
        print('Atualizado periodo para: ' + tarde)
        fb_ultimoperiodo = tarde
    # Manha e/ou tarde preenchidos
    if fb_ultimoperiodo != fechamento:
        print('Horario: ' + ph_horario + ' maior ao: ' + fb_horario)
        print('Ultimo periodo: ' + fb_ultimoperiodo)

        # Caso tenha uma abertura pela manha, colete os dados,
        # em seguida tenha uma suspensao e depois volte com outros dados.
        if fb_ultimoperiodo == manha and int(ph_horario.split(':')[0]) < int('13'):
            print('Parece que houve duas cotacoes para o periodo da manha. fb_ultimoperiodo=' + fb_ultimoperiodo)
            print('Fazendo o update do periodo da: ' + manha)
            periodo = manha
        # Caso tenha uma abertura pela tarde, colete os dados,
        # em seguida tenha uma suspensao e depois volte com outros dados.
        elif fb_ultimoperiodo == tarde and int(ph_horario.split(':')[0]) >= int('13') and int(ph_horario.split(':')[0]) <= int('16'):
            print('Parece que houve duas cotacoes para o periodo da tarde. fb_ultimoperiodo=' + fb_ultimoperiodo)
            print('Fazendo o update do periodo da: ' + tarde)
            periodo = tarde
        else:
            periodo = proximoPeriodo(fb_ultimoperiodo)

        print('Atualizando o periodo: ' + periodo)
        carregaDados(periodo, '')
        atualizaPeriodo(periodo)
        print('Atualizado periodo para: ' + periodo)
        atualizaLastGet(ph_lastget)
    else:
        print('Tentando atualizar dia igual, sendo que o ultimoperiodo ja e fechamento. Olhar arquivo json.')

else:
    print('Dias iguais, horarios iguais. Nada a fazer.')
