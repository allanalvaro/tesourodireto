# !/usr/bin/env python3

from ph2 import ParseHub
import time
import json
import os

timestamp = time.strftime("%d-%m-%Y_%H:%M:%S")
phfile = 'results/run_results-' + timestamp + '.json'

ph = ParseHub('t1CnzXyI5bVez4QDYFa0tBjR')
p1 = ph.projects[0]
r1 = p1.run()

while r1.check_available() != 1:
    print('Waitting run finishing...')
    time.sleep(1)

print('Run finished')
tmp = r1.get_data()

json.dump(tmp, open(phfile,'w'))
print('resultado gravado em: ' + phfile)

os.system('cat ' + phfile + '| sed \'s/R$ //g\' > .tmp')
os.system('cat .tmp | sed \'s/%//g\' >' + phfile)
os.system('rm -f .tmp')
os.system('cp ' + phfile + ' tesouroparser.json')
