# !/usr/bin/env python3

import requests
import json
import re

url = 'https://tesourodireto.firebaseio.com/avisos/'
urlend = '.json?auth=3ACaIBDlNv38no10JV5hMjjs4JV6C5qxggQCwvv9'

inputfile = 'avisosparser.json'

def retiraMes(titulo=None):
    mes = titulo.split(' - ')[0]
    mes = mes.split('/')[1]
    return mes

def retiraTituloMensagem(titulo=None):
    msg = titulo.split(' - ')[1]
    return msg

def carregaAviso(mensagem=None, ano=None, dia=None, titulo=None, mesNome=None, mes=None, fullData=None):
    payload = {'titulo': titulo, 'mensagem': mensagem, 'ano': ano, 'dia': dia, 'mesnome': mesNome, 'mes': mes}
    payload = json.dumps(payload)
    load = requests.patch(url + fullData + '/' + urlend, data=payload)

# load json file from parsehub
with open(inputfile) as data_file:
    mensagens = json.load(data_file)

for aviso in mensagens['avisos']:

    mensagem = aviso['mensagem']
    ano = aviso['ano']
    dia = aviso['dia']
    titulo = aviso['titulo']
    mesNome = aviso['mes']
    mes = retiraMes(titulo)
    fullData = ano + '-' + mes + '-' + dia
    titulo = retiraTituloMensagem(titulo)

    carregaAviso(mensagem, ano, dia, titulo, mesNome, mes, fullData)
