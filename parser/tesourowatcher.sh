#!/bin/bash

CONSULTA_LINK='http://www3.tesouro.gov.br/tesouro_direto/consulta_titulos_novosite/consultatitulos.asp'
ULTIMAATUALIZACAO='.ultimaatualizacao'
TIMESTAMP=$(date +'%d%m%y%H%m%s')
LOG="logs/backend-$TIMESTAMP.log"
#PYTHON='/Library/Frameworks/Python.framework/Versions/3.4/bin/python3'
PYTHON='/usr/bin/python3'
DUMP=".tesourodump"

curl -s $CONSULTA_LINK > $DUMP

SUSPENSO=$(cat $DUMP | grep -i 'fechado no momento\| suspenso no momento')
if [[ $? -eq 0 ]]; then
  echo "Mercado fechado/suspenso no momento!"
  exit 10
fi

ATUALIZACAO=$(cat $DUMP | grep -i Atualizado)
if [[ $? -ne 0 ]]; then
  echo "Problemas acessando o site do tesouro" >> $LOG
  echo "Fazendo nova tentativa em 10 segundos..." >> $LOG
  sleep 10
  ATUALIZACAO=$(curl -s $CONSULTA_LINK | grep -i Atualizado)
  if [[ $? -ne 0 ]]; then
    echo "Ainda com problemas acessando o site do tesouro" >> $LOG
    echo "Abortando ate a proxima execucao" >> $LOG
    cat $LOG | slacktee/slacktee.sh
    exit 1
  fi
fi

FILTRADO=$(echo $ATUALIZACAO | awk '{gsub("<[^>]*>", "")}1' | head -n 1 | awk '{$1=$1}1' | sed 's/ //g')
if [ $FILTRADO == $(cat $ULTIMAATUALIZACAO) ]
then
  echo "Sem mais atualizao de cotacao"
  exit 0
else
  echo "Nova atualizacao disponivel, inicializando o parser..." | slacktee/slacktee.sh
  echo "Executando: parsehub.py..." >> $LOG
   $PYTHON parsehub.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando o parsehub.py" >> $LOG
    echo "Fazendo nova tentativa em 10 segundos..." >> $LOG
    sleep 10
    $PYTHON parsehub.py >> $LOG 2>&1
    if [[ $? -ne 0 ]]; then
      echo "Ainda com problemas executando o parsehub.py" >> $LOG
      echo "Abortando ate a proxima execucao" >> $LOG
      cat $LOG | slacktee/slacktee.sh
      exit 2
    fi
  fi
  echo "Executando: tesouroparser.py..." >> $LOG
  $PYTHON tesouroparser.py >> $LOG 2>&1
  if [[ $? -ne 0 ]]; then
    echo "Problemas executando tesouroparser.py" >> $LOG
    echo "Verificar com URGENCIA!!" >> $LOG
    cat $LOG | slacktee/slacktee.sh
    exit 3
  fi
fi

echo $FILTRADO > $ULTIMAATUALIZACAO

cat $LOG | slacktee/slacktee.sh
echo "Execucao OK" | slacktee/slacktee.sh

